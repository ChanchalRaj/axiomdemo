//
//  SearchCollectionViewExtension.swift
//  AxiomDemo
//
//  Created by Raj on 9/15/20.
//  Copyright © 2020 Khatri LLC. All rights reserved.
//

import UIKit
extension SearchViewController:UICollectionViewDelegate{
    
}
extension SearchViewController:UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let presenter = presenter else {return 0}
        return presenter.totalMobiles()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let productCellClassName = String(describing: MobileCollectionViewCell.self)
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: productCellClassName, for: indexPath) as? MobileCollectionViewCell
        if let product = presenter?.mobileAtIndex(indexPath: indexPath){
            cell?.populateProduct(product: product)
        }
        return cell ?? UICollectionViewCell()
    }
}
extension SearchViewController:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        return collectionView.calculateCellSize(numberOfColumns: 2, flowLayout: flowLayout)
    }
}
