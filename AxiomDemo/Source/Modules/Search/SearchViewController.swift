//
//  SearchViewController.swift
//  AxiomDemo
//
//  Created by Raj on 9/15/20.
//  Copyright © 2020 Khatri LLC. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {

    var presenter:SearchPresenterInterface?
    @IBOutlet weak var productsCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeUI()
    }
    private func initializeUI(){
        let productCellClassName = String(describing: MobileCollectionViewCell.self)
        let nib = UINib(nibName: productCellClassName,bundle: nil)
        productsCollectionView.register(nib, forCellWithReuseIdentifier: productCellClassName)
    }
    func searchKeyword(keyword:String?){
        presenter?.search(with: keyword)
    }
    @IBAction func filterTapped(_ sender: Any) {
        print("Filter tapped")
    }
}
extension SearchViewController:SearchViewControllerInterface,UISearchResultsUpdating,UISearchBarDelegate{
    func updateSearchResults(for searchController: UISearchController) {
        presenter?.search(with: searchController.searchBar.text)
    }
    
    func didFetchSearchResult() {
        DispatchQueue.main.async {
            self.productsCollectionView.reloadData()
        }
    }
}
