//
//  SearchInterfaces.swift
//  AxiomDemo
//
//  Created by Raj on 9/15/20.
//  Copyright © 2020 Khatri LLC. All rights reserved.
//

import Foundation
protocol SearchPresenterInterface{
    
    /// Returns the count of searched mobiles
    func totalMobiles()->Int
    
    /// Mobile at a given index
    /// - Parameter indexPath: index for which the mobile is required
    func mobileAtIndex(indexPath:IndexPath)->Mobile
    
    /// searches mobiles in local DB by the keyword
    /// - Parameter keyword: keyword to search the mobile
    func search(with keyword: String?)
}
protocol SearchViewControllerInterface{
    func didFetchSearchResult()
}
protocol SearchRouterInterface{
}
protocol SearchInteractorInterface{
    func searchMobile(keyword:String)->Result<[Mobile],Error>
}
