//
//  SearchRouter.swift
//  AxiomDemo
//
//  Created by Raj on 9/15/20.
//  Copyright © 2020 Khatri LLC. All rights reserved.
//

import UIKit
struct Filter {
    let keyword:String
    let brand:String
}
final class SearchRouter:BaseRouter{
    // MARK: - Private properties -
    private let storyboard = UIStoryboard(name: "Home", bundle: nil)
    
    // MARK: - Module setup -
    init() {
        let searchViewController = storyboard.instantiateViewController(ofType: SearchViewController.self)
        let searchController = UISearchController(searchResultsController: searchViewController)
        searchController.searchResultsUpdater = searchViewController
        searchController.searchBar.placeholder = "Search Mobile Phones"
        searchController.searchBar.delegate = searchViewController
        searchController.showsSearchResultsController = true
        super.init(viewController: searchController)
        let interactor = SearchInteractor()
        let presenter = SearchPresenter.init(view: searchViewController, interactor: interactor, router: self)
        searchViewController.presenter = presenter
    }
}
extension SearchRouter:SearchRouterInterface{
    
}
