//
//  SearchInteractor.swift
//  AxiomDemo
//
//  Created by Raj on 9/15/20.
//  Copyright © 2020 Khatri LLC. All rights reserved.
//

import Foundation
class SearchInteractor{
    let dbManager:DBManagerInterface
    init(dbManager:DBManagerInterface = DBManager.init()) {
        self.dbManager = dbManager
    }
}
extension SearchInteractor:SearchInteractorInterface{
    func searchMobile(keyword: String) -> Result<[Mobile], Error> {
        let result = dbManager.getMobiles(keyword: keyword)
        switch result {
        case .success(let persistableMobiles):
            let mobiles = persistableMobiles.map{Mobile.init(id: $0.id, brand:$0.brand, phone: $0.phone, picture: $0.picture, sim: $0.sim, resolution: $0.resolution, audioJack: $0.audioJack, gps: $0.gps, battery: $0.battery, priceEur: $0.priceEur)}
            return .success(mobiles)
        case .failure(let error):
            return .failure(error)
        }
    }
}
