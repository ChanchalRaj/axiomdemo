//
//  SearchPresenter.swift
//  AxiomDemo
//
//  Created by Raj on 9/15/20.
//  Copyright © 2020 Khatri LLC. All rights reserved.
//

import Foundation
protocol SearchFilterUpdatable {
    func updateFilter(filter:Filter)
}
class SearchPresenter:SearchFilterUpdatable{
    private weak var view:SearchViewController!
    let interactor:SearchInteractorInterface
    let router:SearchRouterInterface
    var mobiles:[Mobile] = []
    
    //private var selectedFilter:Filter
    
    init(view:SearchViewController,interactor:SearchInteractorInterface = SearchInteractor(),router:SearchRouterInterface) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    private static func filterProducts(products:[Mobile],filter:Filter)->[Mobile]{
        products.filter{$0.phone.localizedCaseInsensitiveContains(filter.keyword) && $0.brand == filter.brand}
    }
    func updateFilter(filter:Filter){
        //self.selectedFilter = filter
        //Update view
    }
}
extension SearchPresenter:SearchPresenterInterface{
    func totalMobiles() -> Int {
        return mobiles.count
    }
    
    func mobileAtIndex(indexPath: IndexPath) -> Mobile {
        return mobiles[indexPath.row]
    }
    func search(with keyword: String?) {
        if let keyword = keyword, !keyword.isEmpty{
            let result = interactor.searchMobile(keyword: keyword)
            switch result {
            case .success(let mobiles):
                self.mobiles = mobiles
                view.didFetchSearchResult()
            case .failure(let error):
                print(error.localizedDescription)
            }
        }else{
            //Empty the tables
        }
    }
}
