//
//  HomeInterfaces.swift
//  AxiomDemo
//
//  Created by Raj on 9/8/20.
//  Copyright © 2020 Khatri LLC. All rights reserved.
//

import Foundation
protocol HomePresenterInterface{
    
    /// Fetches all products from either API or local storage
    func getAllProducts()
    
    /// Total number of brands
    func totalBrands()->Int
    
    /// Brand at a given index
    /// - Parameter index: index for which the brand is required
    func brandAtIndex(index:Int)->String
    
    /// Indicates whether the brands pointed to by the given index is selected or not
    /// - Parameter index: index of the branch for which to check if the it is selected or not
    func isBrandSelectedAtIndex(index:Int) -> Bool
    
    /// Triggers the mobiles list, based on selected brand
    /// - Parameter index: index of the brand selected
    func didSelectBrandAtIndex(index:Int)
    
    /// Total number of mobiles
    func totalMobiles()->Int
    
    /// Mobile at a given index
    /// - Parameter indexPath: index for which the mobile is required
    func mobileAtIndex(indexPath:IndexPath)->Mobile
    
    /// Opens Search Module
    func openSearchController()
}
protocol HomeViewControllerInterface{
    
    /// Triggers when all products are loaded, so that view can reload mobiles
    func didLoadAllProducts()
    
    /// Show errors
    /// - Parameter message: message to show to user
    func showError(message:String)
}
protocol HomeRouterInterface{
    
    /// Opens Search module
    func openSearchController()
}
protocol HomeInteractorInterface{
    
    /// Fetches mobiles either from local DB or from API
    /// - Parameter completion: completion block returns the mobiles
    func getMobiles(completion:@escaping(Result<[Mobile],Error>)->Void)
}
