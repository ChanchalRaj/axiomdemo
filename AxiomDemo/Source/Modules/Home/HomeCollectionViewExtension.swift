//
//  HomeCollectionViewExtension.swift
//  AxiomDemo
//
//  Created by Raj on 9/8/20.
//  Copyright © 2020 Khatri LLC. All rights reserved.
//
import UIKit

extension HomeViewController:UICollectionViewDelegate{
    
}
extension HomeViewController:UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let presenter = presenter else {return 0}
        
        if collectionView == categoriesCollectionView{
            return presenter.totalBrands()
        }else{
            return presenter.totalMobiles()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == categoriesCollectionView{
            let productCategoryCellClassName = String(describing: MobileCategoryCollectionViewCell.self)
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: productCategoryCellClassName, for: indexPath) as? MobileCategoryCollectionViewCell
            if let category = presenter?.brandAtIndex(index: indexPath.row){
                let isSelected = presenter?.isBrandSelectedAtIndex(index: indexPath.row) ?? false
                cell?.configureCategory(category: category,isSelected: isSelected)
            }
            return cell ?? UICollectionViewCell()
        }else{
            let productCellClassName = String(describing: MobileCollectionViewCell.self)
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: productCellClassName, for: indexPath) as? MobileCollectionViewCell
            if let product = presenter?.mobileAtIndex(indexPath: indexPath){
                cell?.populateProduct(product: product)
            }
            return cell ?? UICollectionViewCell()
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == categoriesCollectionView{
            presenter?.didSelectBrandAtIndex(index: indexPath.row)
        }
    }
    
}
extension HomeViewController:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        return collectionView.calculateCellSize(numberOfColumns: 2, flowLayout: flowLayout)
    }
}
