//
//  HomeNetworkDAO.swift
//  AxiomDemo
//
//  Created by Raj on 9/8/20.
//  Copyright © 2020 Khatri LLC. All rights reserved.
//

import Foundation
class HomeInteractor{
    let apiClient:APIClientInterface
    let dbManager:DBManagerInterface
    init(apiClient:APIClientInterface = APIClient(),dbManager:DBManagerInterface = DBManager.init()) {
        self.apiClient = apiClient
        self.dbManager = dbManager
    }
}

extension HomeInteractor:HomeInteractorInterface{
    func getMobiles(completion: @escaping (Result<[Mobile], Error>) -> Void) {
        getMobilesFromAPI { [weak self](result:Result<[Mobile], Error>) in
            switch result{
            case .success(let mobiles):
                completion(.success(mobiles))
            case .failure(_):
                if let interactor = self{
                    completion(interactor.getMobilesFromLocalDB())
                }
            }
        }
    }
    private func getMobilesFromLocalDB()->Result<[Mobile],Error>{
        let localDBResult = dbManager.getData(MobilePersistable.self)
        switch localDBResult {
        case .success(let persistableMobiles):
            let mobiles = persistableMobiles.map{
                Mobile.init(id: $0.id, brand: $0.brand, phone: $0.phone, picture: $0.picture, sim: $0.sim, resolution: $0.resolution, audioJack: $0.audioJack, gps: $0.gps, battery: $0.battery, priceEur: $0.priceEur)
            }
            return .success(mobiles)
        case .failure(let error):
            return .failure(error)
        }
    }
    private func getMobilesFromAPI(completion: @escaping (Result<[Mobile], Error>) -> Void){
        guard let request = APIRouter.getProducts.request() else{return}
        apiClient.performRequest(request: request) { [weak self](result:Result<[Mobile], Error>) in
            switch result{
            case .success(let mobiles):
                self?.deleteAllMobilesFromLocalDB()
                let filteredMobile = mobiles.filter{$0.id != 0 || !$0.brand.isEmpty}
                self?.saveMobilesIntoLocalDB(mobiles: filteredMobile)
                completion(.success(filteredMobile))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    private func saveMobilesIntoLocalDB(mobiles:[Mobile]){
        for mobile in mobiles{
            let mobilePersistable = MobilePersistable.init(id: mobile.id, brand: mobile.brand, phone: mobile.phone, picture: mobile.picture, sim: mobile.sim, resolution: mobile.resolution, audioJack: mobile.audioJack, gps: mobile.gps, battery: mobile.battery, priceEur: mobile.priceEur)
            _ = dbManager.saveData(data: mobilePersistable)
        }
    }
    private func deleteAllMobilesFromLocalDB(){
        _ = dbManager.deleteData(fromTable: MobilePersistable.self)
    }
}
