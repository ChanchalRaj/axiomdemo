//
//  HomeViewController.swift
//  AxiomDemo
//
//  Created by Raj on 9/8/20.
//  Copyright © 2020 Khatri LLC. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    
    var presenter:HomePresenterInterface?
    
    @IBOutlet weak var categoriesCollectionView: UICollectionView!
    @IBOutlet weak var productsCollectionView: UICollectionView!
    //private var searchController:UISearchController?
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeUI()
        presenter?.getAllProducts()
    }
    private func initializeUI(){
        let mobileCellClassName = String(describing: MobileCollectionViewCell.self)
        let nib = UINib(nibName: mobileCellClassName,bundle: nil)
        productsCollectionView.register(nib, forCellWithReuseIdentifier: mobileCellClassName)
        
        let mobileCategoryCellClassName = String(describing: MobileCategoryCollectionViewCell.self)
        let productCategoryCellNib = UINib(nibName: mobileCategoryCellClassName,bundle: nil)
        categoriesCollectionView.register(productCategoryCellNib, forCellWithReuseIdentifier: mobileCategoryCellClassName)
    }
    
    @IBAction func searchTapped(_ sender: Any) {
        presenter?.openSearchController()
    }
}
extension HomeViewController:HomeViewControllerInterface{
    func showError(message: String) {
        print("error: \(message)")
    }
    func didLoadAllProducts() {
        DispatchQueue.main.async {
            self.categoriesCollectionView.reloadData()
            self.productsCollectionView.reloadData()
        }
    }
}
