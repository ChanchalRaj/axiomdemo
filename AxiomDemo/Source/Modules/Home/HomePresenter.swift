//
//  HomePresenter.swift
//  AxiomDemo
//
//  Created by Raj on 9/8/20.
//  Copyright © 2020 Khatri LLC. All rights reserved.
//

import Foundation
class HomePresenter{
    private weak var view:HomeViewController!
    let interactor:HomeInteractorInterface
    let router:HomeRouterInterface
    private var allProducts:[Mobile] = []
    private var selectedBrandProducts:[Mobile] = []
    private var brands:[String] = []
    private var selectedBrandIndex = 0
    init(view:HomeViewController,interactor:HomeInteractorInterface = HomeInteractor(),router:HomeRouterInterface) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
}
extension HomePresenter:HomePresenterInterface{
    func totalBrands() -> Int {
        brands.count
    }
    func brandAtIndex(index: Int) -> String {
        return brands[index]
    }
    func isBrandSelectedAtIndex(index:Int) -> Bool{
        return index == selectedBrandIndex
    }
    func totalMobiles()->Int {
        return selectedBrandProducts.count
    }
    
    func mobileAtIndex(indexPath: IndexPath) -> Mobile {
        return selectedBrandProducts[indexPath.row]
    }
    
    func getAllProducts() {
        interactor.getMobiles { [weak self](result:Result<[Mobile], Error>) in
            switch result{
            case .success(let products):
                self?.allProducts = products
                if let brands = self?.getBrands(products: products){
                    self?.brands = brands
                }
                self?.didSelectBrandAtIndex(index: 0)
                //self?.view?.didLoadAllProducts()
            case .failure(let error):
                self?.view?.showError(message: error.localizedDescription)
            }
        }
    }
    private func getBrands(products:[Mobile])->[String]{
        return Array(Set(products.map{$0.brand})).sorted()
    }
    func didSelectBrandAtIndex(index: Int) {
        selectedBrandIndex = index
        if brands.count > index{
            selectedBrandProducts = allProducts.filter{$0.brand == brands[index]}
            view.didLoadAllProducts()
        }
    }
    func openSearchController() {
        router.openSearchController()
    }
}
