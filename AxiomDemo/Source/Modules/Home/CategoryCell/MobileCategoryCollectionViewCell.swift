//
//  MobileCategoryCollectionViewCell.swift
//  AxiomDemo
//
//  Created by Raj on 9/9/20.
//  Copyright © 2020 Khatri LLC. All rights reserved.
//

import UIKit

class MobileCategoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var labelCategory: UILabel!
    @IBOutlet weak var viewSelectionIndicator: UIView!
    private var category:String?
    
    func configureCategory(category:String,isSelected:Bool){
        self.category = category
        labelCategory.text = category
        viewSelectionIndicator.isHidden = !isSelected
    }
}
