//
//  MobileCollectionViewCell.swift
//  AxiomDemo
//
//  Created by Raj on 9/9/20.
//  Copyright © 2020 Khatri LLC. All rights reserved.
//

import UIKit

class MobileCollectionViewCell: UICollectionViewCell {
    private var product:Mobile?
    @IBOutlet weak var imageProduct: UIImageView!
    @IBOutlet weak var labelProductName: UILabel!
    func populateProduct(product:Mobile){
        labelProductName.text = product.phone
        if let url = URL.init(string: product.picture){
            imageProduct.loadImge(withUrl: url)
        }
        
    }
}
