//
//  MobileDisplayable.swift
//  AxiomDemo
//
//  Created by Raj on 9/9/20.
//  Copyright © 2020 Khatri LLC. All rights reserved.
//

import Foundation
protocol MobileDisplayable {
    var products:[Mobile]{get set}
}
