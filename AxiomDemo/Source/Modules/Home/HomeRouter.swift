//
//  HomeRouter.swift
//  AxiomDemo
//
//  Created by Raj on 9/8/20.
//  Copyright © 2020 Khatri LLC. All rights reserved.
//

import UIKit
final class HomeRouter:BaseRouter{
    // MARK: - Private properties -
    private let storyboard = UIStoryboard(name: "Home", bundle: nil)

    // MARK: - Module setup -
    init() {
        let homeViewControllerNav = storyboard.instantiateViewController(withIdentifier: "HomeViewControllerNav") as! UINavigationController
        let homeViewController = homeViewControllerNav.topViewController as! HomeViewController
        super.init(viewController: homeViewController)
        let interactor = HomeInteractor()
        let presenter = HomePresenter.init(view: homeViewController, interactor: interactor, router: self)
        homeViewController.presenter = presenter
    }

}
extension HomeRouter:HomeRouterInterface{
    func openSearchView(configure:()){
        let searchView = SearchRouter.init()
        viewController.presentWireframe(searchView, animated: true, completion: nil)
    }
    func openSearchController() {
        let searchModuleRouter = SearchRouter.init()
        viewController.present(searchModuleRouter.viewController, animated: true, completion: nil)
    }
}
