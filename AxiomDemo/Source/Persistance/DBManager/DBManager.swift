//
//  DBManager.swift
//  AxiomDemo
//
//  Created by Raj on 9/19/20.
//  Copyright © 2020 Khatri LLC. All rights reserved.
//

import Foundation
import GRDB

protocol DBManagerInterface{
    func getData<T:FetchableRecord & TableRecord>(_ fromTable:T.Type)->Result<[T],Error>
    func saveData(data:MutablePersistableRecord)->Result<Bool,Error>
    func deleteData<T:MutablePersistableRecord>(fromTable:T.Type)->Result<Bool,Error>
    func getMobiles(keyword:String)->Result<[MobilePersistable],Error>
}

class DBManager:DBManagerInterface {
    
    // The shared database queue
    static var dbQueue: DatabasePool!
    
    static func openDatabase() throws -> DatabasePool {
        // Connect to the database
        let databaseURL = try FileManager.default
            .url(for: .applicationSupportDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            .appendingPathComponent("Axiom.sqlite")
        let dbQueue = try DatabasePool(path: databaseURL.path)
        
        // Define the database schema
        try migrator.migrate(dbQueue)
        
        return dbQueue
    }
    /// The DatabaseMigrator that defines the database schema.
    ///
    /// See https://github.com/groue/GRDB.swift/blob/master/README.md#migrations
        static var migrator: DatabaseMigrator {
            var migrator = DatabaseMigrator()
            migrator.registerMigration("createMobile") { db in
                // Create mobile table
                try db.create(table: "mobile") { t in
                    t.column("id",.integer).primaryKey()
                    t.column("brand", .text)
                    t.column("phone", .text)
                    t.column("picture", .text)
                    t.column("sim", .text)
                    t.column("resolution", .text)
                    t.column("audioJack", .text)
                    t.column("gps", .text)
                    t.column("battery", .text)
                    t.column("priceEur", .integer)
                }
            }
            return migrator
        }
    static func setupDatabase() throws {
        dbQueue = try DBManager.openDatabase()
    }
    func getData<T:FetchableRecord & TableRecord>(_ fromTable:T.Type)->Result<[T],Error>{
        guard let queue = DBManager.dbQueue else {return Result.failure(CustomError.generalError())}
        do {
            let records = try queue.read{db in
                try fromTable.fetchAll(db)
            }
            return Result.success(records)
        } catch (let error) {
            return Result.failure(error)
        }
    }
    func saveData(data:MutablePersistableRecord)->Result<Bool,Error>{
        do {
            try DBManager.dbQueue.write{db in
                var data = data
                try data.save(db)
            }
            return .success(true)
        }catch (let error){
            print("Error 3: \(error.localizedDescription)")
            return .failure(error)
        }
    }
    func deleteData<T:MutablePersistableRecord>(fromTable:T.Type)->Result<Bool,Error>{
        do {
            _ = try DBManager.dbQueue.write{db in
                try fromTable.deleteAll(db)
            }
            return.success(true)
        }catch (let error){
            print("Error : \(error.localizedDescription)")
            return .failure(error)
        }
    }
    func getMobiles(keyword: String)->Result<[MobilePersistable],Error> {
        //let filter = MobilePersistable.filter(MobilePersistable.Columns.phone.like("\(keyword)"))
        do {
            let mobiles = try DBManager.dbQueue.read({ (db) -> [MobilePersistable] in
                //try filter.fetchAll(db)
                try MobilePersistable.fetchAll(db)
            })
            let filteredMobiles = mobiles.filter{
                guard let _ = $0.phone.range(of: keyword,options: .caseInsensitive) else {
                    return false
                }
                return true
            }
            return .success(filteredMobiles)
        } catch (let error) {
            return .failure(error)
        }
    }
}
