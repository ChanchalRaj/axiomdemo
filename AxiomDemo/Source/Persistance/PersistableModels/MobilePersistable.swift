//
//  MobilePersistable.swift
//  AxiomDemo
//
//  Created by Raj on 9/18/20.
//  Copyright © 2020 Khatri LLC. All rights reserved.
//

import Foundation
import GRDB

struct MobilePersistable{
    let id: Int
    let brand, phone: String
    let picture: String
    let sim, resolution, audioJack: String
    let gps, battery: String
    let priceEur: Int
    static let databaseTableName = "mobile"
//    enum Columns: String, ColumnExpression {
//        //public static let phone = Column(CodingKeys.phone)
//        case id,brand,phone,picture,sim,resolution,audioJack,gps,battery,priceEur
//    }
    enum Columns {
       static let phone = Column(CodingKeys.phone)
    }
}

extension MobilePersistable:Codable, FetchableRecord, MutablePersistableRecord{
    
}
