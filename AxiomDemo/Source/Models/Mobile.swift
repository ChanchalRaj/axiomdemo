//
//  Mobile.swift
//  AxiomDemo
//
//  Created by Raj on 9/7/20.
//  Copyright © 2020 Khatri LLC. All rights reserved.
//

import Foundation
// MARK: - Mobile
struct Mobile: Codable {
    internal init(id: Int, brand: String, phone: String, picture: String, sim: String, resolution: String, audioJack: String, gps: String, battery: String, priceEur: Int) {
        self.id = id
        self.brand = brand
        self.phone = phone
        self.picture = picture
        self.sim = sim
        self.resolution = resolution
        self.audioJack = audioJack
        self.gps = gps
        self.battery = battery
        self.priceEur = priceEur
    }
    
    let id: Int
    let brand, phone: String
    let picture: String
    let sim, resolution, audioJack: String
    let gps, battery: String
    let priceEur: Int
    
    init(from decoder:Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        if let productId = try? values.decode(Int.self, forKey:.id){
            id = productId
        }else{
            //If id is not of Int type, assign 0 to avoid Decoder from stop in between
            //Ideally, the test case should fail and the API JSON response shouldn't sent unexpected response types
            //But for demo purpose, I am letting it go, furthermore, I will filter the products, having 0 as id.
            id = 0
        }
        brand = try values.decode(String.self, forKey:.brand)
        phone = try values.decode(String.self, forKey:.phone)
        picture = try values.decode(String.self, forKey:.picture)
        sim = try values.decode(String.self, forKey:.sim)
        resolution = try values.decode(String.self, forKey:.resolution)
        audioJack = try values.decode(String.self, forKey:.audioJack)
        gps = try values.decode(String.self, forKey:.gps)
        battery = try values.decode(String.self, forKey:.battery)
        if let productPrice = try? values.decode(Int.self, forKey:.priceEur){
            priceEur = productPrice
        }else{
            priceEur = 0
        }
    }
}
