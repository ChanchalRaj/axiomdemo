//
//  UICollectionView+CellSize.swift
//  AxiomDemo
//
//  Created by Raj on 9/15/20.
//  Copyright © 2020 Khatri LLC. All rights reserved.
//

import UIKit
extension UICollectionView{
    func calculateCellSize(numberOfColumns:Int,flowLayout:UICollectionViewFlowLayout)->CGSize{
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(numberOfColumns - 1))
        let size = Int((bounds.width - totalSpace) / CGFloat(numberOfColumns))
        return CGSize(width: size, height: size)
    }
}
