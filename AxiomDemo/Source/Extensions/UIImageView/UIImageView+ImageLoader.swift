//
//  UIImageView+ImageLoader.swift
//  AxiomDemo
//
//  Created by Raj on 9/14/20.
//  Copyright © 2020 Khatri LLC. All rights reserved.
//

import UIKit
import Kingfisher
extension UIImageView {
    
    /// Loads image in UIImageView with the given URL
    /// - Parameter url: url of the image to load into ImageView
    func loadImge(withUrl url: URL) {
        self.kf.setImage(with: url)
    }
}
