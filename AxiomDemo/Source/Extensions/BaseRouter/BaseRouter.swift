//
//  BaseRouter.swift
//  AxiomDemo
//
//  Created by Raj on 9/9/20.
//  Copyright © 2020 Khatri LLC. All rights reserved.
//
import UIKit

protocol BaseRouterInterface: class {
}

class BaseRouter {

    private unowned var _viewController: UIViewController
    
    //to retain view controller reference upon first access
    private var _temporaryStoredViewController: UIViewController?

    init(viewController: UIViewController) {
        _temporaryStoredViewController = viewController
        _viewController = viewController
    }

}

extension BaseRouter: BaseRouterInterface {
    
}

extension BaseRouter {
    
    var viewController: UIViewController {
        defer { _temporaryStoredViewController = nil }
        return _viewController
    }
    
    var navigationController: UINavigationController? {
        return viewController.navigationController
    }
    
}

extension UIViewController {
    
    func presentWireframe(_ router: BaseRouter, animated: Bool = true, completion: (() -> Void)? = nil) {
        present(router.viewController, animated: animated, completion: completion)
    }
    
}

extension UINavigationController {
    
    func pushWireframe(_ router: BaseRouter, animated: Bool = true) {
        self.pushViewController(router.viewController, animated: animated)
    }
    
    func setRootRouter(_ router: BaseRouter, animated: Bool = true) {
        self.setViewControllers([router.viewController], animated: animated)
    }
    
}
