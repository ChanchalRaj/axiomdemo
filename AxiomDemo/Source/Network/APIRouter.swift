//
//  APIRouter.swift
//  AxiomDemo
//
//  Created by Raj on 9/8/20.
//  Copyright © 2020 Khatri LLC. All rights reserved.
//

import Foundation

enum Enviroment{
    case sandbox
    case production
    
    var baseURL:String{
        switch self{
        case .sandbox:
            return API.Axiom.sandbox
        case .production:
            return API.Axiom.production
        }
    }
}
enum APIRouter {
    case getProducts
    // MARK: - HTTPMethod
    static var environment:Enviroment{
        #if DEBUG
        return .sandbox
        #endif
    }
    var apiVersion:String{
        switch self {
        default:
            return ""
        }
    }
    var method: HTTPMethod {
        switch self {
        case .getProducts:
            return .get
        }
    }
    
    
    // MARK: - Path
    private var path: String {
        switch self {
        case .getProducts:
            return "b/5f59cf8a7243cd7e82397478/latest"
        }
    }
    
    // MARK: - Parameters
    private var parameters: Encodable? {
        switch self {
        case .getProducts:
            return nil
        }
    }
    // MARK: - Headers
    private var headers:[String:String]?{
        switch self {
        default: return ["".s.e.c.r.e.t.dash.k.e.y:"".`$`._2.b.`$`._1._0.`$`.Z.e.N.q.v.G.S.g.d.N.k.d.E.P.K._4.K.V.U.V.x.e.b.z._1.u.F.P.V.J.B.R.S.E.M.dot.l.dot.l.S.forward_slash.G.j._5.O._0.u._0.H.c.y.B.q]
        
        }
    }
    // MARK: - URLRequestConvertible
    private func asURLRequest() throws -> URLRequest {
        let url = try (APIRouter.environment.baseURL + apiVersion).asURL().appendingPathComponent(path)
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        request.allHTTPHeaderFields = headers
        request.timeoutInterval = TimeInterval(30)
        if let parameters = self.parameters,let paramData = parameters.toJSONData(){
            request.httpBody = paramData
        }
        return request
    }
    func request()->URLRequest?{
        guard let request = try? asURLRequest() else {
            return nil
        }
        return request
    }
}

