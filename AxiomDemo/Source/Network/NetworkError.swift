//
//  NetworkError.swift
//  AxiomDemo
//
//  Created by Raj on 9/8/20.
//  Copyright © 2020 Khatri LLC. All rights reserved.
//

import Foundation

enum NetworkError{
    case unAuthorized(String)
    case unprocessableEntity(String)
    case general(String)
    static func error(statusCode:Int,validRange:Range<Int> = 200..<401)->NetworkError?{
        if !validRange.contains(statusCode){
            if statusCode == 401{
                return self.unAuthorized("Unauthorized Access")
            } else if statusCode == 422{
                return self.unprocessableEntity("There is some problem, we are looking into it")
            }else{
                return self.general("There is some problem, we are looking into it")
            }
        }else{
            return nil
        }
    }
}
extension NetworkError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .unAuthorized(let message):
            return NSLocalizedString(message, comment: "")
        case .general(let message):
            return NSLocalizedString(message, comment: "")
        case .unprocessableEntity(let message):
            return NSLocalizedString(message, comment: "")
        }
    }
}
