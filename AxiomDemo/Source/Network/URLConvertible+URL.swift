//
//  URLConvertible+URL.swift
//  AxiomDemo
//
//  Created by Raj on 9/8/20.
//  Copyright © 2020 Khatri LLC. All rights reserved.
//

import Foundation
extension String {
    /// Returns a `URL` if `self` can be used to initialize a `URL` instance, otherwise throws.
    ///
    /// - Returns: The `URL` initialized with `self`.
    /// - Throws:  An `CustomError` instance.
    public func asURL() throws -> URL {
        guard let url = URL(string: self) else { throw CustomError.error("Invalid URL")}

        return url
    }
}
