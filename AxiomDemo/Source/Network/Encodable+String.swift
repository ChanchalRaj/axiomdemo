//
//  Encodable+String.swift
//  AxiomDemo
//
//  Created by Raj on 9/8/20.
//  Copyright © 2020 Khatri LLC. All rights reserved.
//

import Foundation
extension Encodable{
    func encodeToString()->String?{
        do {
            let encoder = JSONEncoder()
            let jsonData = try encoder.encode(self)
            guard let jsonString = String(data: jsonData, encoding: String.Encoding.utf8) else {
                return nil
            }
            return jsonString
        } catch _ {
            return nil
        }
    }
}
