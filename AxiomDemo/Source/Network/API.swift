//
//  API.swift
//  AxiomDemo
//
//  Created by Raj on 9/8/20.
//  Copyright © 2020 Khatri LLC. All rights reserved.
//

import Foundation

struct API{
    struct Axiom{
        static let sandbox = "https://api.jsonbin.io"
        static let production = "https://api.jsonbin.io"
    }
}
