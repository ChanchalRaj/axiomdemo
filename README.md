# Axiom Demo
An online mobile shopping app

## Getting Started

Clone the project and just run the project in XCode

### Prerequisites

XCode 11.0 or above

### Running
- Run 'pod install' Terminal command in the project directory
- Navigate to project directory and open project using *AxiomDemo.xcworkspace*
- Build and run the application on simulator or actual device running iOS 13.0 or later using Xcode 11.0 or greater.

## User Guidelines
- On home screen, all the mobiles fetched from API are shown
- To search a product, tap on the search button on top right corner and type

## Technical Notes
- For Architecture purpose VIPER is used
- Depedencies are injected through protocol to ease testing and write decoupled code
- Documentation of VIPER module's interfaces are written
- Models for persistance are made seperate from the ones being used to show to user - so that if the persistance strategy is changed, it shouldn't affect all the app

## Third Party
- Kingfisher third-party library is used to cache images
- GRDB.swift third-party libary is used to manage persistance

## Improvements
- Pagination needs to be implemented to save memory
- Those mobiles which user has already swipped above, can be taken off from memory and if user scrolls back, can be used fetched again to avoid large in-memory storage
- Filters are to be implemented but due to less time couldn't be done
- Test cases are to be written but due to less time and busy schedule couldn't be done
- UI Improvements can be done like showing appropriate messages and showing data fetch animations

## Sample Screens
![Home Screen](Screenshots/Home.png)
![Search Screen](Screenshots/Search.png)

## Versioning

Version 1.0
For more information on versioning, see [Semantic Versioning](http://semver.org/).

## Authors

* **Chanchal Raj** - (https://bitbucket.org/ChanchalRaj/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

